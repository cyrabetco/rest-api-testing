Feature: Totv Search DSB

  Background:
    * url baseUrl
    * param query = 'DE000C11T2J0'
    * param pageNum = '0'
    * param pageSize = '1'
    * param totv = 'true'

  Scenario: Check DE000C11T2J0 if ToTV
    Given path '/search'
    And header Authorization = 'Basic Y3lyYS5iZXRjb0Bhbm5hLWRzYi5jb206UGFzc3dvcmQ4OA=='
    And header X-DSB-Client = '1'
    And header DSB-Token = 'X7y8C3d3'
    When method GET
    * def DERIVED = get[0] response..ToTV-record.Derived
    * match DERIVED contains { ToTV : true}
    And status 200