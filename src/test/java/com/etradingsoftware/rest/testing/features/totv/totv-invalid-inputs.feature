Feature: ToTV Invalid Inputs

  Background:
    * url baseUrl
    * param query = 'EZ6VHSP2F3J3'
    * param totv = 'true'

  Scenario: PageSize is zero
    Given path '/search'
    And header Authorization = 'Basic Y3lyYS5iZXRjb0Bhbm5hLWRzYi5jb206UGFzc3dvcmQ4OA=='
    And header X-DSB-Client = '1'
    And header DSB-Token = 'X7y8C3d3'
    * param pageNum = '0'
    * param pageSize = '0'
    When method GET
    Then status 400
    And match response.message == 'Invalid pageSize (must be <= 1000)'

  Scenario: PageNum are letters
    Given path '/search'
    And header Authorization = 'Basic Y3lyYS5iZXRjb0Bhbm5hLWRzYi5jb206UGFzc3dvcmQ4OA=='
    And header X-DSB-Client = '1'
    And header DSB-Token = 'X7y8C3d3'
    * param pageNum = 'AA'
    * param pageSize = '1'
    When method GET
    Then status 400
    And match response.message == 'Unable to parse as number: AA'

  Scenario: PageNum is negative number
    Given path '/search'
    And header Authorization = 'Basic Y3lyYS5iZXRjb0Bhbm5hLWRzYi5jb206UGFzc3dvcmQ4OA=='
    And header X-DSB-Client = '1'
    And header DSB-Token = 'X7y8C3d3'
    * param pageNum = '-3'
    * param pageSize = '1'
    When method GET
    Then status 400
    And match response.message == 'Invalid pageNum (must be a whole number)'

  Scenario: PageSize is letter
    Given path '/search'
    And header Authorization = 'Basic Y3lyYS5iZXRjb0Bhbm5hLWRzYi5jb206UGFzc3dvcmQ4OA=='
    And header X-DSB-Client = '1'
    And header DSB-Token = 'X7y8C3d3'
    * param pageNum = '0'
    * param pageSize = 'AA'
    When method GET
    Then status 400
    And match response.message == 'Unable to parse as number: AA'

  Scenario: PageSize is negative number
    Given path '/search'
    And header Authorization = 'Basic Y3lyYS5iZXRjb0Bhbm5hLWRzYi5jb206UGFzc3dvcmQ4OA=='
    And header X-DSB-Client = '1'
    And header DSB-Token = 'X7y8C3d3'
    * param pageNum = '0'
    * param pageSize = '-3'
    When method GET
    Then status 400
    And match response.message == 'Invalid pageSize (must be <= 1000)'

