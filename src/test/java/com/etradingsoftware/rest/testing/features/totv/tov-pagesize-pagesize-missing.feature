Feature: Totv Search DSB

  Background:
    * url baseUrl
    * param query = 'EZ6VHSP2F3J2'
    * param totv = 'true'

  Scenario: PageSize is negative number
    Given path '/search'
    And header Authorization = 'Basic Y3lyYS5iZXRjb0Bhbm5hLWRzYi5jb206UGFzc3dvcmQ4OA=='
    And header X-DSB-Client = '1'
    And header DSB-Token = 'X7y8C3d3'
    When method GET
    Then status 200