Feature: Totv Search DSB

  Background:
    * url baseUrl
    * param query = 'EZ6VHSP2F3J2'
    * param pageNum = '0'
    * param pageSize = '1'
    * param totv = 'true'

  Scenario: Search EZ6VHSP2F3J2
    Given path '/search'
    And header Authorization = 'Basic Y3lyYS5iZXRjb0Bhbm5hLWRzYi5jb206UGFzc3dvcmQ4OA=='
    And header X-DSB-Client = '1'
    And header DSB-Token = 'X7y8C3d3'
    When method GET
    * def record = get[0] response..ToTV-record
    * match record contains { DSB-ISIN : '#notnull'}
    And status 200