@ignore
Feature: stateful mock server

  Background:
    * configure cors = true

  Scenario: pathMatches('/fipro/search') && methodIs('get') && paramValue('query') == 'EZ6VHSP2F3J2' && paramValue('totv') == 'true' && paramValue('pageNum') == '0' && paramValue('pageSize') == '1'
    * def response = {query: EZ6VHSP2F3J2, pageNum: 0, pageSize: 1, totalResults: 1, records:[{"ToTV-record":{"Header":{"ISIN":'EZ6VHSP2F3J2', lastCompletedProcessingDate:'2018-09-18',lastModifiedDate:'2018-07-20', cfiCategory:F, cfiGroup:C},"DSB-ISIN": {"Header": {"AssetClass": "Foreign_Exchange","InstrumentType": "Forward","UseCase": "Forward","Level": "InstRefDataReporting"},"Attributes": {"NotionalCurrency": "CAD","ExpiryDate": "2019-03-18","OtherNotionalCurrency": "NOK","PriceMultiplier": 1,"DeliveryType": "PHYS"},"ISIN": {"ISIN": "EZ6VHSP2F3J2","Status": "New","StatusReason": "","LastUpdateDateTime": "2017-12-05T15:27:44"}},"Derived":{"ToTV":true, totvDate:'2018-01-23', utotv:false, utotvDate:'null'}}}], responseCode:200}

  Scenario: pathMatches('/fipro/search') && methodIs('get') && paramValue('query') == 'DE000C11T2J0' && paramValue('totv') == 'true' && paramValue('pageNum') == '0' && paramValue('pageSize') == '1'
    * def response = {query: DE000C11T2J0, pageNum: 0, pageSize: 1, totalResults: 1, records:[{"ToTV-record":{"Header":{"ISIN":'DE000C11T2J0', lastCompletedProcessingDate:'2018-09-18',lastModifiedDate:'2018-07-20', cfiCategory:F, cfiGroup:C},"Firds-RefData":{"RM":[{"TermntdRcrd": {"DerivInstrmAttrbts": {"UndrlygInstrm": {"Sngl": {"LEI": "529900J0JGLSFDWNFC20"}}}}}]},  "Firds-TransparencyData": [{"2017-01-01:2017-12-31": {"NonEqtyTrnsprncyData": {"RptgPrd": {"FrDtToDt": {"ToDt": "2017-12-31","FrDt": "2017-01-01"}},"PstTradLrgInScaleThrshld": {"Amt": {"Ccy": "EUR","content": 2500000}},"PreTradLrgInScaleThrshld": {"Amt": {"Ccy": "EUR","content": 1000000}},"PstTradInstrmSzSpcfcThrshld": {"Amt": {"Ccy": "EUR","content": 1500000}},"PreTradInstrmSzSpcfcThrshld": {"Amt": {"Ccy": "EUR","content": 300000}},"Id": "KR6097242830"}}},{"default": {"NonEqtyTrnsprncyData": {"Lqdty": false,"Id": "KR6097242830"}}}],"Derived":{"ToTV":true, totvDate:'2018-01-23', utotv:false, utotvDate:'null'}}}], responseCode:200}

  Scenario: pathMatches('/fipro/search') && methodIs('get') && paramValue('query') == 'EZ6VHSP2F3J2' && paramValue('totv') == 'true'
    * def responseStatus = 200

  Scenario: pathMatches('/fipro/search') && methodIs('get') && paramValue('query') == 'EZ6VHSP2F3J3' && paramValue('totv') == 'true' && paramValue('pageNum') == 'AA' && paramValue('pageSize') == '1'
    * def response = {"responseCode": 400,"message": "Unable to parse as number: AA"}
    * def responseStatus = 400

  Scenario: pathMatches('/fipro/search') && methodIs('get') && paramValue('query') == 'EZ6VHSP2F3J3' && paramValue('totv') == 'true' && paramValue('pageNum') == '-3' && paramValue('pageSize') == '1'
    * def responseStatus = 400
    * def response = {"responseCode": 400,"message": "Invalid pageNum (must be a whole number)"}

  Scenario: pathMatches('/fipro/search') && methodIs('get') && paramValue('query') == 'EZ6VHSP2F3J3' && paramValue('totv') == 'true' && paramValue('pageNum') == '0' && paramValue('pageSize') == 'AA'
    * def responseStatus = 400
    * def response = {"responseCode": 400,"message": "Unable to parse as number: AA"}

  Scenario: pathMatches('/fipro/search') && methodIs('get') && paramValue('query') == 'EZ6VHSP2F3J3' && paramValue('totv') == 'true' && paramValue('pageNum') == '0' && paramValue('pageSize') == '-3'
    * def responseStatus = 400
    * def response = {"responseCode": 400,"message": "Invalid pageSize (must be <= 1000)"}

  Scenario: pathMatches('/fipro/search') && methodIs('get') && paramValue('query') == 'EZ6VHSP2F3J3' && paramValue('totv') == 'true' && paramValue('pageNum') == '0' && paramValue('pageSize') == '0'
    * def responseStatus = 400
    * def response = {"responseCode": 400,"message": "Invalid pageSize (must be <= 1000)"}

  Scenario:
      # catch-all
    * def responseStatus = 404
    * def responseHeaders = { 'Content-Type': 'text/html; charset=utf-8' }
    * def response = <html><body>Not Found</body></html>