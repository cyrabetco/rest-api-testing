 function() {

  var url;
  var basePath;
  var basePort;
  if (karate.env == 'local') {
    url = 'http://localhost'
    basePath = '/fipro'
    basePort = '8080';
  } else if (karate.env == 'dev') {
    url = 'http://dev.anna-dsb.com'
    basePath = '/api'
    basePort = '';
  }

  var port = karate.properties['karate.server.port'];
  port = port || basePort;
  return { baseUrl: url + ":" + port + basePath }
 }